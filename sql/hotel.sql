DROP SCHEMA IF EXISTS hotel;
CREATE SCHEMA hotel DEFAULT CHARACTER SET utf8;
USE hotel;

-- KREIRANJE TABELA
CREATE TABLE soba (
	id INT AUTO_INCREMENT,	
	tipSobe VARCHAR(20) NOT NULL,
	brojKreveta INT(9) NOT NULL,
	cena DECIMAL (10,2) NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE rezervacija (
	id INT AUTO_INCREMENT, 
	soba INT NOT NULL,
	ulazak DATETIME NOT NULL,
	izlazak DATETIME NOT NULL,
	imeIPrezime VARCHAR(35) NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY (soba) REFERENCES soba(id)
);

INSERT INTO soba (id, tipSobe,brojKreveta,cena) VALUES (1, 'Studio', 2,2000);
INSERT INTO soba (id, tipSobe,brojKreveta,cena) VALUES (2, 'Suite', 1,2500);
INSERT INTO soba (id, tipSobe,brojKreveta,cena) VALUES (3, 'Family room', 4,3500);
INSERT INTO soba (id, tipSobe,brojKreveta,cena) VALUES (4, 'Interconected rooms', 2,2500);
INSERT INTO soba (id, tipSobe,brojKreveta,cena) VALUES (5, 'Interconected rooms', 2,2000);
INSERT INTO soba (id, tipSobe,brojKreveta,cena) VALUES (6, 'Suite', 2,3000);

INSERT INTO rezervacija (id,soba,ulazak,izlazak,imeIPrezime) VALUES (1,6, '2017-11-01 12:00:00' ,'2017-11-10 10:00:00', 'Petar Pertovic');
INSERT INTO rezervacija (id,soba,ulazak,izlazak,imeIPrezime) VALUES (2,3, '2017-11-05 13:00:00' ,'2017-11-10 08:00:00', 'Marko Markovic');
INSERT INTO rezervacija (id,soba,ulazak,izlazak,imeIPrezime) VALUES (3,6, '2017-11-19 03:00:00' ,'2017-11-22 03:00:00', 'Jovan Jovanovic');
INSERT INTO rezervacija (id,soba,ulazak,izlazak,imeIPrezime) VALUES (4,3, '2017-11-10 12:30:00' ,'2017-11-20 07:00:00', 'Petar Pertovic');
INSERT INTO rezervacija (id,soba,ulazak,izlazak,imeIPrezime) VALUES (5,4, '2017-11-10 12:00:00' ,'2017-12-02 08:00:00', 'Marko Markovic');
INSERT INTO rezervacija (id,soba,ulazak,izlazak,imeIPrezime) VALUES (6,5, '2017-11-10 12:00:00' ,'2017-12-02 08:00:00', 'Marko Markovic');
