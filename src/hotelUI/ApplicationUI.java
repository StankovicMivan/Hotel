package hotelUI;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import PomocnaKlasa.PomocnaKlasa;

public class ApplicationUI {

	public static void ispisiTekstOsnovneOpcije(){	
		System.out.println("Hotel - Osnovne opcije:");
		System.out.println("\tOpcija broj 1 - Rad sa sobama");
		System.out.println("\tOpcija broj 2 - Rad sa rezervacijama");
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ IZ PROGRAMA");	
	}
	public static void main(String[] args) {


		Connection conn;
		try {

			Class.forName("com.mysql.jdbc.Driver");

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/hotel?useSSL=false", "root","root");
		} catch (Exception ex) {
			System.out.println("Neuspela konekcija na bazu!");
			ex.printStackTrace();

			return;
		}
		//ocitava bazu i smesta je u liste koje se nalaze u PopuniListe klasi

		int odluka = -1;
		while(odluka!= 0){
			ApplicationUI.ispisiTekstOsnovneOpcije();
			System.out.print("opcija:");
			odluka = PomocnaKlasa.ocitajCeoBroj();
			switch (odluka) {
			case 0:	
				System.out.println("Izlaz iz programa");	
				break;
			case 1:
				SobaUI.meni(conn);
				break;
			case 2:
				RezervacijeUI.meni(conn);
				break;

			default:
				System.out.println("Nepostojeca komanda");
				break;
			}
		}

		//zatvaranje konekcije
		try {
			conn.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
}