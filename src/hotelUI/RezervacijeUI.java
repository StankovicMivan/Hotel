package hotelUI;

import java.sql.Connection;
import java.util.ArrayList;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import DAO.RezervacijeDAO;
import PomocnaKlasa.PomocnaKlasa;

import model.Rezervacija;
import model.Soba;



public class RezervacijeUI {

	public static void meni(Connection conn) {
		String odluka = "";
		while (!odluka.equals("x")) {
			ispisiMenu2();
			System.out.print("opcija:");
			odluka = PomocnaKlasa.ocitajTekst();
			switch (odluka) {				
			case "1":
				prikaz(conn);
				break;
			case "2":
				prikazPeriod(conn);
				break;
			case "3" :
				izmenaDatumaIVremena(conn);
				break;
			case "4" :
				//				ispisiSveNastavnike(conn);
				break;	
			case "x":
				System.out.println("Izlaz");
				break;
			default:
				System.out.println("Nepostojeca komanda");
				break;

			}
		}
	}

	public static void ispisiMenu2() {
		System.out.println();
		System.out.println("Meni:");
		System.out.println("\t1 - Prikaz svih rezervacija");
		System.out.println("\t2 - Prikaz rez za datum");
		System.out.println("\t3 - Izmena rezervacije");
		//		System.out.println("\t3 - Brisanje Nastavnika");
		//		System.out.println("\t4 - Prikaz Nastavnika");

		System.out.println("\tx - Vrati se nazad");
	}
	private static void prikaz(Connection conn) {
		List<Rezervacija> rezervacije = RezervacijeDAO.getAll(conn);
		for (int i = 0; i < rezervacije.size(); i++) {
			System.out.println(rezervacije.get(i));
		}
	}
	
	@SuppressWarnings("deprecation")
	private static void prikazPeriod(Connection conn) {
		List<Rezervacija> rezervacije = RezervacijeDAO.getAll(conn);

		System.out.println("Unesi donju granicu");
		Date dg = PomocnaKlasa.ocitajDatum();
		Timestamp donjaGranica = new Timestamp(dg.getTime());
		System.out.println("Unesi gornju granicu");
		Date gg = PomocnaKlasa.ocitajDatum();
		Timestamp gornjaGranica = new Timestamp(gg.getTime());
	
//		Timestamp test1 = new Timestamp(donjaGranica.getTime());
		gornjaGranica.setHours(23);
		gornjaGranica.setMinutes(59);
		gornjaGranica.setSeconds(59);
	
		while(donjaGranica.after(gornjaGranica)){
			System.out.println("gonja granica mora biti posle donje granice");
			gg = PomocnaKlasa.ocitajDatum();
			gornjaGranica = new Timestamp(gg.getTime());
		}
		List<Rezervacija> rezervacijeZaPeriod = new ArrayList<Rezervacija>();
	
		for (Rezervacija rez : rezervacije) {
			
			if(rez.rezervacijaPripadaOpesegu(donjaGranica, gornjaGranica) ||
					rez.rezervacijaDelimicnoPripadaOpesegu(donjaGranica, gornjaGranica)){
				rezervacijeZaPeriod.add(rez);
			}
		}
		System.out.println();
		System.out.println("ID Soba Ulazak\t\t        Izlazak\t\t        Ime i Prezime");
		System.out.println("-----------------------------------------------------------------------");
		ispisiRezervacije(rezervacijeZaPeriod);
		System.out.println();
	
	}

	private static void ispisiRezervacije(List<Rezervacija> rezervacijeZaPeriod) {
		for (int i = 0; i < rezervacijeZaPeriod.size(); i++) {
			System.out.println(rezervacijeZaPeriod.get(i));
		}
		
	}
	
	public static Rezervacija pronadjiRezervaciju(Connection conn) {
		List<Rezervacija> rezervacije = RezervacijeDAO.getAll(conn);
		Rezervacija rez = null;

		while (rez == null) {
			System.out.println("Unesite id rezervacije: ");
			int id = PomocnaKlasa.ocitajCeoBroj();
			for (Rezervacija rezervacija2 : rezervacije) {
				if (id == rezervacija2.getId()) {
					rez = rezervacija2;
				}
			}
		}
		return rez;
	}
	public static void izmenaDatumaIVremena(Connection conn) {
		Rezervacija rezervacijaZamenjanje = pronadjiRezervaciju(conn);
		
		Soba soba = rezervacijaZamenjanje.getSoba();
		List<Rezervacija> rezervacije = RezervacijeDAO.getAllForSoba(conn, soba);
		
		
		ispisiRezervacije(rezervacije);
		
		System.out.println("Unesi datum ulaska");
		Timestamp datumUlaska = PomocnaKlasa.ocitajDatumIVreme();
		System.out.println("Unesi datum izlaska");
		Timestamp datumIzlaska = PomocnaKlasa.ocitajDatumIVreme();

		while(datumUlaska.after(datumIzlaska)){
			System.out.println("datum izlaska mora biti posle datuma ulaska");
			datumIzlaska =  PomocnaKlasa.ocitajDatumIVreme();
		}
		
		boolean mogucaPromena = true;
		for (Rezervacija rez : rezervacije) {
			
			if (!rezervacijaZamenjanje.equals(rez)){
				if (rez.rezervacijaObuhvataOpseg(datumUlaska, datumIzlaska) ||
						rez.rezervacijaDelimicnoObuhvataOpeseg(datumUlaska, datumIzlaska)){
					mogucaPromena = false;
					break;
				} 
			}
		}
		
		if(mogucaPromena){
			rezervacijaZamenjanje.setUlazak(datumUlaska);
			rezervacijaZamenjanje.setIzlazak(datumIzlaska);
			RezervacijeDAO.update(conn, rezervacijaZamenjanje);
			System.out.println("Rezervacija uspeno izmenjena");
		}else {
			System.out.println("Nije moguce izvrsiti promenu");
		}
	}
}	
