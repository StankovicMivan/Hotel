package DAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Soba;





public class SobeDAO {
	public static List<Soba> getAll(Connection conn) {
		List<Soba> sobe = new ArrayList<>();

		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT * FROM soba";

			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);

			while (rset.next()) {
				int index = 1;
				int id = rset.getInt(index++);

				String tipSobe = rset.getString(index++);
				int brojKreveta =rset.getInt(index++);
				double cena = rset.getDouble(index++);
				
				Soba soba = new Soba(id,tipSobe,brojKreveta,cena);
				sobe.add(soba);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return sobe;
	}

	public static Soba getSobaByID(Connection conn, int id) {
		Soba soba = null;


		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id,tipSobe,brojKreveta,cena FROM soba WHERE id = " + id;

			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);

			if (rset.next()) {
				int index = 1;
				int id1 = rset.getInt(index++);
				
				String tipSobe = rset.getString(index++);
				int brojKreveta = rset.getInt(index++);
				double cena = rset.getDouble(index++);
				
				soba = new Soba(id1,tipSobe,brojKreveta,cena);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return soba;
	}
}
