package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

import java.util.List;


import model.Rezervacija;
import model.Soba;




public class RezervacijeDAO {

	public static List<Rezervacija> getAll(Connection conn) {
		List<Rezervacija> rezervacije = new ArrayList<>();

		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT * FROM rezervacija";

			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);

			while (rset.next()) {
				int index = 1;
				int id = rset.getInt(index++);
				int soba = rset.getInt(index++);
				Timestamp ulazak = rset.getTimestamp(index++);
				Timestamp izlazak = rset.getTimestamp(index++);

				String imeIPRezime = rset.getString(index++);

				Rezervacija rezervacija = new Rezervacija(id, SobeDAO.getSobaByID(conn, soba),ulazak,izlazak,imeIPRezime);
				rezervacije.add(rezervacija);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return rezervacije;
	}
	public static List<Rezervacija> getAllForSoba(Connection conn, Soba so) {
		ArrayList<Rezervacija> retVal = new ArrayList<Rezervacija>();

		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			String query = "SELECT id, soba, ulazak, izlazak, imeIPrezime FROM rezervacija WHERE soba = ?";
			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setInt(index++, so.getId());
			rset = pstmt.executeQuery();
			while (rset.next()) {
				index = 1;
				int id = rset.getInt(index++);
				Soba soba = SobeDAO.getSobaByID(conn, rset.getInt(index++));
				Timestamp ulazak = rset.getTimestamp(index++);
				Timestamp izlazak = rset.getTimestamp(index++);
				String ImeIprezime = rset.getString(index++);
				retVal.add(new Rezervacija(id, soba, ulazak, izlazak, ImeIprezime));

			}
			rset.close();
			pstmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		so.setRezervacije(retVal);
		return retVal;
	}
	public static boolean update(Connection conn, Rezervacija rezervacijaZamenjanje) {
		PreparedStatement pstmt = null;

		try {
			String query = "UPDATE rezervacija SET  ulazak = ?, izlazak = ? WHERE id = ?";
			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setTimestamp(index++, rezervacijaZamenjanje.getUlazak());
			pstmt.setTimestamp(index++, rezervacijaZamenjanje.getIzlazak());
			pstmt.setInt(index++, rezervacijaZamenjanje.getId());

			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		return false;
	}

}

