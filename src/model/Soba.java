package model;

import java.util.ArrayList;

public class Soba {

	private int id;
	private String tipSobe;
	private int brojKreveta;
	private double cena;
	private ArrayList<Rezervacija> rezervacije = new ArrayList<>();
	
	public Soba() {
		
	}
	
	public Soba(int id, String tipSobe, int brojKreveta, double cena, ArrayList<Rezervacija> rezervacije) {
		super();
		this.id = id;
		this.tipSobe = tipSobe;
		this.brojKreveta = brojKreveta;
		this.cena = cena;
		this.rezervacije = rezervacije;
	}

	public Soba(int id, String tipSobe, int brojKreveta, double cena) {
		super();
		this.id = id;
		this.tipSobe = tipSobe;
		this.brojKreveta = brojKreveta;
		this.cena = cena;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTipSobe() {
		return tipSobe;
	}
	public void setTipSobe(String tipSobe) {
		this.tipSobe = tipSobe;
	}
	public int getBrojKreveta() {
		return brojKreveta;
	}
	public void setBrojKreveta(int brojKreveta) {
		this.brojKreveta = brojKreveta;
	}
	public double getCena() {
		return cena;
	}
	public void setCena(double cena) {
		this.cena = cena;
	}
	
	public ArrayList<Rezervacija> getRezervacije() {
		return rezervacije;
	}

	public void setRezervacije(ArrayList<Rezervacija> rezervacije) {
		this.rezervacije = rezervacije;
	}

	@Override
	public String toString() {
		return id + " " + tipSobe + ", brojKreveta=" + brojKreveta + ", cena=" + cena;
	}

	
}
