package model;


import java.sql.Date;
import java.sql.Timestamp;



public class Rezervacija {


	private int id;
	private Soba soba;
	private Timestamp ulazak;
	private Timestamp izlazak;
	private String imePrezime;

	public Rezervacija() {

	}

	public Rezervacija(int id, Soba soba, Timestamp ulazak, Timestamp izlazak, String imePrezime) {
		super();
		this.id = id;
		this.soba = soba;
		this.ulazak = ulazak;
		this.izlazak = izlazak;
		this.imePrezime = imePrezime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Soba getSoba() {
		return soba;
	}

	public void setSoba(Soba soba) {
		this.soba = soba;
	}

	public Timestamp getUlazak() {
		return ulazak;
	}

	public void setUlazak(Timestamp ulazak) {
		this.ulazak = ulazak;
	}

	public Timestamp getIzlazak() {
		return izlazak;
	}

	public void setIzlazak(Timestamp izlazak) {
		this.izlazak = izlazak;
	}

	public String getImePrezime() {
		return imePrezime;
	}

	public void setImePrezime(String imePrezime) {
		this.imePrezime = imePrezime;
	}

	@Override
	public String toString() {


		return id + "   " + soba.getId() + "   " + ulazak + "   " + izlazak+ "   " + imePrezime ;
	}

	public boolean rezervacijaPripadaOpesegu(Timestamp donjaGranica,Timestamp gornjaGranica){
		boolean pripada = false;
//		sSystem.out.println(id);
		Date dateDonja = new Date(donjaGranica.getTime());
		Date dateGornja = new Date(gornjaGranica.getTime());
//		System.out.println(dateDonja + " " + dateGornja);
		Date dateUlazak = new Date(ulazak.getTime());
		Date dateIzlazak = new Date(izlazak.getTime());


		
//		System.out.println(dateDonja + " " + dateUlazak);
//		System.out.println(dateDonja.compareTo(dateUlazak) );

	

		// |donjaGranica| -ulazak- -izlazak- |goranjGranica|

		if(dateGornja.equals(dateIzlazak)   ) {
			pripada = true;//u celosti
		}
		if(dateDonja.equals(dateUlazak)) {
			pripada = true;
		}
		if(dateGornja.equals(dateUlazak)) {
			pripada = true;
		}
		if(dateDonja.equals(dateIzlazak)) {
			pripada = true;
		}

		if( dateDonja.after(dateUlazak) && dateGornja.before(dateIzlazak)) {

			return true;
		}
		if( dateDonja.before(dateUlazak) && dateGornja.after(dateIzlazak)) {

			return true;
		}
		return pripada;
	}

	public boolean rezervacijaDelimicnoPripadaOpesegu(Timestamp donjaGranica,Timestamp gornjaGranica){

		Date dateDonja = new Date(donjaGranica.getTime());
		Date dateGornja = new Date(gornjaGranica.getTime());

		Date dateUlazak = new Date(ulazak.getTime());
		Date dateIzlazak = new Date(izlazak.getTime());


		boolean pripada = false;
		// |donjaGranica| -ulazak- |gornjaGranica|  -izlazak-

		if((dateUlazak.before(dateGornja) && dateIzlazak.after(dateGornja))) {
			return true;

		}
		if((dateUlazak.before(dateDonja) && dateIzlazak.after(dateDonja))) {
			return true;

		}

		if(dateDonja.equals(dateUlazak)) {
			pripada = true;
		}
		if(dateDonja.equals(dateIzlazak)) {
			pripada = true;
		}
		if(dateGornja.equals(dateUlazak)) {
			pripada = true;
		}
		
		if(dateGornja.equals(dateIzlazak)   ) {
			pripada = true;//u celosti
		}
		
		
	
		
		
		//		if((donjaGranica.before(ulazak) && ulazak.before(gornjaGranica))) {
		//			return true;
		//		};		
		//		// -ulazak- |donjaGranica|  -izlazak- |gornjaGranica|
		//		if( (donjaGranica.before(izlazak) && izlazak.before(gornjaGranica))){
		//			return true;
		//		}
		return pripada;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rezervacija other = (Rezervacija) obj;
		if (id != other.id)
			return false;
		return true;
	}
	public boolean rezervacijaObuhvataOpseg(Timestamp donjaGranica, Timestamp gornjaGranica){
		boolean pripada = false;
		// -ulazak- |donjaGranica| |goranjGranica| -izlazak- 
		Date dateDonja = new Date(donjaGranica.getTime());
		Date dateGornja = new Date(gornjaGranica.getTime());

		Date dateUlazak = new Date(ulazak.getTime());
		Date dateIzlazak = new Date(izlazak.getTime());

		
		pripada = dateUlazak.before(dateDonja) && dateIzlazak.after(dateGornja); //u celosti
		return pripada;
	}
	
	public boolean rezervacijaDelimicnoObuhvataOpeseg(Timestamp donjaGranica, Timestamp gornjaGranica){
		boolean pripada = false;
		
		// -ulazak- |donjaGranica| |goranjGranica| -izlazak- 
				Date dateDonja = new Date(donjaGranica.getTime());
				Date dateGornja = new Date(gornjaGranica.getTime());

				Date dateUlazak = new Date(ulazak.getTime());
				Date dateIzlazak = new Date(izlazak.getTime());

		// -ulazak- |donjaGranica|  -izlazak-
		pripada |= (dateUlazak.before(dateDonja) && dateDonja.before(dateIzlazak));		
		// -ulazak- |gornjaGranica|  -izlazak-
		pripada |= (dateUlazak.before(dateGornja) && dateGornja.before(dateIzlazak));
		return pripada;
	}
}


